# Changelog
All notable changes to this project are documented in this file. The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.0] - 2024-09-17

### Added
- Support of Python 3.10, 3.11, 3.12.
- Support of Django >= 4.2, < 5.1.

### Removed
- Support of Python < 3.10.
- Support of Django <= 4.1.

## [1.1.0] - 2023-01-25

### Added
- Catch exception InvalidVatNumber.
- Add INVALID_INPUT into StatusCode.

### Changed
- New version verify-vat-number 2.0.0.

## [1.0.0] - 2022-06-16

### Added
- Django Verify VAT ID/REG number implementation for ARES and VIES registry.


[Unreleased]: https://gitlab.nic.cz/django-apps/django-verify-vat-number/-/compare/2.0.0...main
[2.0.0]: https://gitlab.nic.cz/django-apps/django-verify-vat-number/-/compare/1.1.0...2.0.0
[1.1.0]: https://gitlab.nic.cz/django-apps/django-verify-vat-number/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.nic.cz/django-apps/django-verify-vat-number/-/compare/ec1886eff8...1.0.0
